require('dotenv').config()
const express = require('express')
const cors = require('cors')
const axios = require('axios')
const _get = require('lodash/get')
const { formatTemparature } = require('./src/utils')
const { response } = require('express')

const app = express()
const port = 3030

app.get('/current-weather', cors(), async (req, res) => {
  const { region } = req.query

  try {
    const response = await axios.get('http://api.openweathermap.org/data/2.5/weather', {
      params: {
        q: `${region},nz`,
        units: 'metric',
        APPID: process.env.OWM_KEY

      },
    })
    const {
      coord,
      main,
      name: locationName,
      weather,
    } = response.data

    const props = {
      coord,
      locationName,
      temparature: {
        current: formatTemparature(main.temp),
        high: formatTemparature(main.temp_max),
        low: formatTemparature(main.temp_min),
      },
      weather: weather[0],
    }

    res.status(200).send(props)
  } catch (error) {
    console.log(error);
    const { cod, message } = _get(error, 'response.data')
    if (cod === '404') res.status(404).send(`Location "${region}" not found in Aotearoa New Zealand.`)
    res.status(response.status).send(message)
  }
})

app.get('/extended-forecast', cors(), async (req, res) => {
  const { lat, lon } = req.query
  try {
    const response = await axios.get('http://api.openweathermap.org/data/2.5/onecall', {
      params: {
        lat,
        lon,
        units: 'metric',
        exclude: 'current,minutely,hourly',
        APPID: process.env.OWM_KEY
      },
    })

    res.status(200).send(response.data)
  } catch (error) {
    const { message } = _get(error, 'response.data')
    res.status(response.status).send(message)
  }
})


app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})