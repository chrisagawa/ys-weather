import _get from 'lodash/get';
import { useHistory, useLocation } from 'react-router-dom'
import { Button } from 'antd'
import { getExtendedForecast } from '../apiHelper'

function Current() {
  const location = useLocation()
  const history = useHistory()
  if (!_get(location, 'state.currentWeather')) {
    history.push('/')
  }
  const {
    coord,
    locationName,
    temparature,
    weather,
  } = location.state.currentWeather

  const onClick = async () => {
    try {
      const response = await getExtendedForecast(coord)
      history.push('/extended', { forecast: response, locationName })
    } catch (error) {
      console.error('💔', error);
    }

  }

  return (
    <div className="container">
      <main className="main">
        <h1 className="title">
          { locationName }
        </h1>
        <div className="current-weather">
          <div className="current-weather__details">
            <h2>{ weather.main }</h2>
            <h3>{ temparature.current }</h3>
            <h3>High { temparature.high }</h3>
            <h3>Low { temparature.low }</h3>
            <br />
            <Button type="primary" onClick={ onClick }>See 7 day forcast</Button>
          </div>
          <div className="current-weather__image">
            <img src={`http://openweathermap.org/img/wn/${weather.icon}@2x.png`} alt={weather.description}/>
          </div>
        </div>
      </main>
    </div>
  );
}

export default Current;