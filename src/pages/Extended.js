import _get from 'lodash/get';
import { useHistory, useLocation } from 'react-router-dom'
import { Row } from 'antd'
import DayCard from '../components/DayCard'

function Extended() {

  const location = useLocation()
  const history = useHistory()
  if (!_get(location, 'state.forecast')) {
    history.push('/')
  }

  const {
    forecast,
    locationName,
  } = location.state

  return (
    <div className="container">
      <main className="main">
        <h1 className="title">
          { locationName }
        </h1>
        <Row gutter={16} justify="center">
          { forecast.daily.map((day, i) => {
              if (i === 0 ) return null
              return <DayCard key={day.dt} details={day} />
            }
          )}
        </Row>
      </main>
    </div>
  );
}

export default Extended;