import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Typography } from 'antd'
import { getCurrentWeather } from '../apiHelper'
import SearchField from '../components/SearchField'

const { Text } = Typography

function Home() {
  const history = useHistory()

  const onSearch = async region => {
    if (region.length < 2) return setErrorMessage('Please enter an NZ city or region.')
    const response = await getCurrentWeather(region)
    if (response.weather) {
      setCurrentWeather(response)
    } else {
      setErrorMessage(response)
    }
  }

  const [currentWeather, setCurrentWeather] = useState(undefined)
  const [errorMessage, setErrorMessage] = useState(undefined)

  useEffect(() => {
    if (currentWeather) {
      setErrorMessage(undefined)
      history.push('/current', { currentWeather })
    }
  }, [currentWeather, history])
  return (
    <div className="container">
      <main className="main">
        <h1 className="title">
          Weather Search
        </h1>

        <div>
          <SearchField onSearch={ onSearch } />
        </div>
        {errorMessage && <Text type="danger">{ errorMessage }</Text>}

      </main>
    </div>
  );
}

export default Home;