import axios from 'axios'
import _get from 'lodash/get'

export const getCurrentWeather = async (region) => {
  try {
    const response = await axios.get('http://localhost:3030/current-weather', {
      params: { region },
    })
    return response.data
  } catch (error) {
    const errorMessage = _get(error, 'response.data')
    return errorMessage
  }
}

export const getExtendedForecast = async (coord) => {
  const { lat, lon } = coord

  try {
    const response = await axios.get('http://localhost:3030/extended-forecast', {
      params: {
        lat,
        lon,
      }
    })
    return response.data
  } catch (error) {
    const errorDetails = _get(error, 'response.data')
    console.error('💔', errorDetails);
    return errorDetails.message
  }
}