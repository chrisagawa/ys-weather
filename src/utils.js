const formatTemparature = value => `${Math.round(value)}°C`

const formatWeekday = timestamp => new Date(timestamp * 1000).toLocaleString("en-nz", { weekday: "long" })

module.exports = {
  formatTemparature,
  formatWeekday,
}