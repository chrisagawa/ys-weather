import { Input } from 'antd'

const { Search } = Input;

const SearchField = ({ onSearch }) => (
  <Search
    className="search-field" 
    placeholder="NZ city or region"
    onSearch={ onSearch }
    maxLength={ 85 }
  />
)

export default SearchField