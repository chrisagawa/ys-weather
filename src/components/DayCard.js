import { Card, Col } from 'antd'
import { formatTemparature, formatWeekday } from '../utils'

const DayCard = ({ details }) => {
  const { dt, temp, weather } = details

  return (
    <Col align="center">
      <Card className="day-card" title={ formatWeekday(dt) }>
        <img src={`http://openweathermap.org/img/wn/${weather[0].icon}@2x.png`} alt={weather[0].description}/>
        <h2>{ weather[0].main }</h2>
        <h3>High { formatTemparature(temp.max) }</h3>
        <h3>Low { formatTemparature(temp.min) }</h3>
      </Card>
    </Col>
  )
}

export default DayCard