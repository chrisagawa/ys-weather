import './App.scss'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import Home from './pages/Home'
import Current from './pages/Current'
import Extended from './pages/Extended'

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/current">
            <Current />
          </Route>
          <Route path="/extended">
            <Extended />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
